[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/pkoperek/pytorch-dnn-evolution)

[![pipeline status](https://gitlab.com/pkoperek/pytorch-dnn-evolution/badges/master/pipeline.svg)](https://gitlab.com/pkoperek/pytorch-dnn-evolution/commits/master)

## torch-dnnevo 

This module provides a framework for experimentation with DNN evolution. It is 
based on great foundation built by projects like `pytorch`, `DEAP` and 
`celery`. Evaluation of individual DNNs can scheduled on a cluster of machines.
Kubernetes deployment descriptors are provided in the `deploy` directory.

A simple web UI is provided to control the progress of evolution. All
individuals are stored in sqlite for further examination.

## Using gitpod.io

Since the development environment is cleanly replicated as a container, this is a recommended approach
to development. To open an instance of gitpod IDE, please click this link: 
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/pkoperek/pytorch-dnn-evolution)
and simply follow the steps to create an account. Since the project is open-source you don't have 
to worry about costs :)

### Using docker in gitpod.io

* Start the workspace.
* In a terminal tab run `$ sudo docker-up` and wait until it initializes (if necessary 
  create a new terminal with "Terminal -> New Terminal" command in the menu).
* Open a new terminal (e.g. use the "Terminal -> New Terminal" command in the menu). 
  In this new terminal all docker commands (`docker`, `docker-compose` etc) should work

Note: in most recent version of `.gitpod.yml` file the above should be automated, however if 
that does not work, you can also prep up the workspace manually.

## Offline development setup

* Install `docker` and `docker-compose`
* You can launch all of the containers with `docker-compose up`
* Changes to driver code trigger automatic reload of the driver webapp - makes
  it easier to develop UI.

Project is automatically setup if you are using `direnv`.

To be able to run unit tests install deps with: `pip3 install -r requirements.txt`

## MiniKube setup

* Install minikube
* Start minikube cluster: `$ minikube start`
* Deploy app to minikube: `$ minikube deploy -f tools/kubernetes/dnnevo.yml`

## Sample experiment

Uses 4 fitness predictors of size 100, the main population is also small: 4
individuals with size multiplier of 1. Learning is conducted over 1 iteration.
Expect very low classification accuracy (e.g. 10%).

```json
{
  "fp_crossover_probability": "0.5", 
  "fp_individual_size": "4",
  "fp_mutation_probability": "0.02", 
  "fp_population_size": "4",
  "fp_train_config": {
    "batch_size": "128", 
    "dataset": "mnist", 
    "learning_rate": "0.1", 
    "momentum": "0.0", 
    "optimizer": "sgd", 
    "rng_seed": "-1",
    "dense_size_multiplier": "1",
    "convolution_size_multiplier": "1",
    "train_iterations": "1"
  },
  "main_crossover_probability": "0.5", 
  "main_individual_size": "4",
  "main_mutation_probability": "0.02", 
  "main_population_size": "4",
  "main_train_config": {
    "batch_size": "128", 
    "dataset": "mnist", 
    "learning_rate": "0.1", 
    "momentum": "0.0", 
    "optimizer": "sgd", 
    "rng_seed": "-1",
    "dense_size_multiplier": "1",
    "convolution_size_multiplier": "1",
    "train_iterations": "1"
  }, 
  "stale_iterations_cnt": "5"
}
```

## Architecture

The system consists of following parts:

* The library responsible for performing the evolution (the `lib/` directory).
* The driver application (the `ui/` directory).
* The workers cluster (jobs are executed through Celery; as per
  https://github.com/docker-library/celery/issues/1#issuecomment-287655769 the
  official way to create a Celery-enabled worker Docker containers is to just
  add Celery dep to app container and run the container with a different
  command). Link to official, deprecated celery image:
  `https://hub.docker.com/_/celery/`. The worker is also a part of the `ui/` component.