VERSION:=$(shell echo '`git rev-parse --short HEAD`')

jupyter:
	docker run -it --rm -v ${PWD}:/srv -p 8081:8080 pkoperek/pytorch-dnnevo-dev:latest

docker-main: 
	git rev-parse --short HEAD > version
	docker build -f Dockerfile -t pkoperek/pytorch-dnnevo:latest -t pkoperek/pytorch-dnnevo:${VERSION} .
	rm -f version

docker-dev:
	docker build -f tools/Dockerfile.dev -t pytorch-dnnevo-dev:latest .

docker: docker-main
	docker-compose up #--scale worker=2

version:
	@echo Version: ${VERSION}
