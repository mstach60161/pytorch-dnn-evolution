import * as React from 'react';
import {ExperimentStatus} from './ExperimentStatus';
import {SocketContextProps, withSocketContext} from '../../common/index';

interface State {
    codeVersion: string,
    name: string,
    status: string,
    active: boolean,
    lastUpdate: string,
}

const emptyExperiment = {
    active: false,
    codeVersion: 'Unknown',
    name: 'Unknown',
    status: 'Unknown',
    lastUpdate: 'Unknown'
};

class ExperimentStatusContainer extends React.Component<SocketContextProps, State> {
    public constructor(props: any) {
        super(props);
        this.state = {
            ...emptyExperiment,
        };
        this.props.socket.on("exp_status", this.handleStatusUpdate);
    }

    public render() {
        return (
            <ExperimentStatus
                codeVersion={this.state.codeVersion}
                name={this.state.name}
                status={this.state.status}
                active={this.state.active}
                stop={this.stopExperiment}
            />
        );
    }

    public componentWillUnmount() {
        this.props.socket.off("exp_status", this.handleStatusUpdate);
    }

    private handleStatusUpdate = (message: any) => {
        this.setState({
            name: message.experiment_name,
            status: message.status,
            active: message.status !== 'NOT_STARTED' && message.status !== 'FINISHED',
            lastUpdate: message.update_timestamp,
            codeVersion: message.code_version
        });
    }

    private stopExperiment = () => {
        this.props.socket.emit("exp_stop");
    }
}

export default withSocketContext(ExperimentStatusContainer);
