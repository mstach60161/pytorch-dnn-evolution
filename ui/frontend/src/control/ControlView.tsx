import * as React from "react";
import ExperimentStatusContainer from "./status/ExperimentStatusContainer";
import { NewExperimentContainer } from "./new-experiment";

interface ControlViewProps {
  notUsed?: string;
}

export const ControlView: React.StatelessComponent<ControlViewProps> = props => {
  return (
    <React.Fragment>
      <ExperimentStatusContainer />
      <NewExperimentContainer />
    </React.Fragment>
  );
};
