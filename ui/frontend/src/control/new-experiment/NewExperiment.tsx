import * as React from 'react';
import { Grid, Row, Button } from 'react-bootstrap';
import '../Control.css';
import {CreateMenu} from "./create-menu/CreateMenu";

interface State {
    createMenuOpen: boolean
}

interface Props {
    createExperiment: (config: any, name: string, maxIterations: number) => void
}

export class NewExperiment extends React.Component<Props, State> {
    public constructor(props: any) {
        super(props);
        this.state = {
            createMenuOpen: false
        };
    }

    public render() {
        return (
            <Grid className="NewExperiment-container">
                <Row>
                    <h3>New Experiment</h3>
                </Row>
                <Row>
                    {
                        !this.state.createMenuOpen ?
                            <Button bsSize="small" bsStyle="primary" className="NewExperiment-button" onClick={this.openCreateMenu}>
                                CREATE
                            </Button> :
                            <CreateMenu
                                onClose={this.closeCreateMenu}
                                createExperiment={this.props.createExperiment}
                            />
                    }
                </Row>
            </Grid>
        );
    }

    private openCreateMenu = () => this.setState({createMenuOpen: true});
    private closeCreateMenu = () => this.setState({createMenuOpen: false});
}
