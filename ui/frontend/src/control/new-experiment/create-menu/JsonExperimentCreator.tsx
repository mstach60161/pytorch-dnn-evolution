import * as React from 'react';
import '../../Control.css';
import { Grid, Row, Button, Col, FormGroup, ControlLabel, FormControl, } from 'react-bootstrap';
import Dropzone, { ImageFile } from 'react-dropzone';
import { configValid } from '../../../common/validators';
import {ErrorModal} from "../../../common/ErrorModal";

interface Props {
    createExperiment: (config: any, name: string, maxIterations: number) => void,
    onClose: () => void,
}

export class JsonExperimentCreator extends React.Component<Props, any> {
    private dropzoneRef: React.RefObject<Dropzone>;

    public constructor(props: any) {
        super(props);
        this.dropzoneRef = React.createRef();
        this.state = {
            error: {
                message: '',
                show: false,
            },
            json: '',
            maxIterations: '',
            name: '',
        };
    }

    public render() {
        return (
            <Grid className="JsonExperimentCreator-container">
                <Row>
                    <Col md={6} sm={12}>
                        <FormGroup
                            controlId="jsonField"
                            // tslint:disable-next-line
                            validationState={this.jsonValidationState()}
                        >
                            <ControlLabel>
                                JSON Config
                            </ControlLabel>
                            <FormControl
                                componentClass="textarea"
                                style={{
                                    height: '200px',
                                    resize: 'none',
                                    width: '100%',
                                }}
                                value={this.state.json}
                                placeholder="Enter json config..."
                                onChange={this.onConfigChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={6} sm={12}>
                        <div className="JsonExperimentCreator-drop-container">
                            <Dropzone
                                ref={this.dropzoneRef}
                                accept='application/json'
                                onDropAccepted={this.onFileUploadAccept}
                                onDropRejected={this.onFileUploadReject}
                                className="JsonExperimentCreator-dropzone"
                            >
                                <p className="JsonExperimentCreator-drop-title">
                                    Drop config file or click to upload...
                                </p>
                            </Dropzone>
                        </div>
                    </Col>
                </Row>
                <Row style={{paddingTop: '10px'}}>
                    <Col md={3}>
                        <FormGroup
                            controlId="nameField"
                            // validationState={this.nameValidationState()}
                        >
                            <ControlLabel>
                                Experiment Name
                            </ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.name}
                                placeholder="Enter name..."
                                onChange={this.onNameChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup
                            controlId="iterField"
                            // tslint:disable-next-line
                            validationState={this.iterValidationState()}
                        >
                            <ControlLabel>
                                Max iterations
                            </ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.maxIterations}
                                placeholder="Enter max iterations count..."
                                onChange={this.onMaxIterchange}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <hr />
                <Row>
                    <Button
                        bsSize="small"
                        bsStyle="primary"
                        className="JsonExperimentCreator-button"
                        onClick={this.createExperiment}
                    >
                        ACCEPT
                    </Button>
                    <Button
                        bsSize="small"
                        bsStyle="secondary"
                        className="JsonExperimentCreator-button"
                        onClick={this.props.onClose}
                    >
                        CANCEL
                    </Button>
                </Row>
                <div>
                    <ErrorModal
                        show={this.state.error.show}
                        title="Error"
                        message={this.state.error.message}
                        onClose={this.hideError}
                    />
                </div>
            </Grid>
        );
    }

    private onConfigChange = (ev: any) => {
        this.setState({json: ev.target.value});
    }

    private onNameChange = (ev: any) => {
        this.setState({name: ev.target.value});
    }

    private onMaxIterchange = (ev: any) => {
        this.setState({maxIterations: ev.target.value});
    }

    private iterValidationState = () => {
        if (this.state.maxIterations === '') {
            return null;
        }
        if (/\D/.test(this.state.maxIterations)) {
            return 'error';
        }
        return 'success';
    }

    // TS compiler complains for some reason, TODO: investigate
    // private nameValidationState = () => {
    //     if (this.state.name.length > 0) {
    //         return 'success';
    //     }
    //     return null;
    // }

    private jsonValidationState = () => {
        if (this.state.json === '') {
            return null;
        }
        try {
            if (!configValid(JSON.parse(this.state.json))) {
                return 'error';
            }
        } catch (err) {
            return 'error';
        }
        return 'success';
    }

    private hideError = () => {
        this.setState({
            error: {
                message: '',
                show: false,
            }
        });
    }

    private showError = (message: string) => {
        this.setState({
            error: {
                message,
                show: true
            }
        });
    }

    private onFileUploadAccept = (accepted: ImageFile[]): void => {
        if (accepted.length !== 1) {
            this.showError('Please upload one file');
        }

        const file = accepted[0];

        const reader = new FileReader();
        reader.onload = () => {
            const jsonString = reader.result;
            // TODO: validate
            this.setState({
                json: JSON.stringify(JSON.parse(jsonString), null, 4),
            });
        };
        reader.onabort = () => this.showError('File reading error');
        reader.onerror = () => this.showError("File reading error");

        reader.readAsText(file);
    }

    private onFileUploadReject = (rejected: ImageFile[]): void => {
        if (rejected.length !== 1) {
            this.showError('Please upload one file');
        }
        const file = rejected[0];
        this.showError(`Invalid file type: ${file.type}`);
    }

    private createExperiment = (): void =>  {
        this.props.createExperiment(
            this.state.json,
            this.state.name,
            this.state.maxIterations
        );
        this.props.onClose();
    }
}
