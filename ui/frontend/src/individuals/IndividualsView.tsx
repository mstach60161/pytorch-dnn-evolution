import * as React from 'react';

interface Props {
    individuals: any[]
}

export const IndividualsView: React.StatelessComponent<Props> = (props) => {
    return (
        <div className="table-responsive">
            <table className="table table-striped table-bordered table-hover model-list">
                <thead>
                <tr>
                    <th className="col-md-1">&nbsp;</th>
                    <th className="column-header col-id">Id</th>
                    <th className="column-header col-fitness">Fitness</th>
                    <th className="column-header col-genotype">Genotype</th>
                </tr>
                </thead>
                <tbody>
                {props.individuals.map((individual, idx) => {
                    return (
                        <tr key={idx}>
                            <td className="list-buttons-column">
                                <a className="icon"
                                   href={`/individuals/${individual.id}`}
                                   title="View Record">
                                    <span className="fa fa-eye glyphicon glyphicon-eye-open"/>
                                </a>
                            </td>
                            <td className="col-id">{individual.id}</td>
                            <td className="col-fitness">{individual.fitness}</td>
                            <td className="col-genotype">{individual.genotype}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
};
