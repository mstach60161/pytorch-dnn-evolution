/* tslint:disable */
import {Validator} from 'jsonschema';

const validator = new Validator();

const probabilityProperty = {
  type: 'string',
  minimum: 0.0,
  maximum: 1.0
}

const positiveIntProperty = {
  type: 'string',
  minimum: 0
}

const trainConfigProperties = {
  batch_size: positiveIntProperty,
  dataset: {
    type: 'string'
  },
  learning_rate: {
    type: 'string'
  },
  momentum: {
    type: 'string'
  },
  optimizer: {
    type: 'string'
  },
  rng_seed: {
    type: 'string'
  },
  dense_size_multiplier: {
    type: 'string'
  },
  convolution_size_multiplier: {
    type: 'string'
  },
  train_iterations: positiveIntProperty
}

const configSchema = {
  id: '/Config',
  type: 'object',
  properties: {
    fp_crossover_probability: {
      anyOf: [
        probabilityProperty,
        {
          type: 'string',
          minimum: 0,
          maximum: 1  // :v
        }
      ]
    },
    fp_individual_size: positiveIntProperty,
    fp_mutation_probability: probabilityProperty,
    fp_population_size: positiveIntProperty,
    fp_train_config: {
      type: 'object',
      properties: trainConfigProperties
    },
    main_crossover_probability: probabilityProperty,
    main_individual_size: positiveIntProperty,
    main_mutation_probability: probabilityProperty,
    main_population_size: positiveIntProperty,
    main_train_config: {
      type: 'object',
      properties: trainConfigProperties
    },
    stale_iterations_cnt: positiveIntProperty
  }
}

export const configValid = (config) => {
  const result = validator.validate(config, configSchema);
  // TODO: detailed validation error handling
  console.log(result.errors);
  return result.valid;
}
