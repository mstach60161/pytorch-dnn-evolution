import * as React from 'react';

export interface SocketContextProps {
    socket?: SocketIOClient.Socket
}

export const SocketContext: React.Context<SocketContextProps> = React.createContext({
    socket: undefined
});


export function withSocketContext<P extends SocketContextProps>(
    WrappedComponent: React.ComponentType<P>
): React.ComponentType<P> {
    return (props: P) => {
        return (
            <SocketContext.Consumer>
                {(socketContext) => <WrappedComponent {...props} {...socketContext}/>}
            </SocketContext.Consumer>
        );
    };
}
