import * as React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';
import ControlViewContainer from './control';
import routing from './routing';
import {CurrentViewContainer} from './current';
import ExperimentsViewContainer from './experiments';
import IndividualsViewContainer from './individuals';
import DatabaseViewContainer from './database';
import {
    SocketContext,
    SocketContextProps
} from './common';
import * as io from 'socket.io-client';
import {Navigation} from './Navigation';
import IndividualViewContainer from "./individual";

import './App.css';

interface AppState {
    socketContext: SocketContextProps
}

class App extends React.Component<{}, AppState> {
    constructor(props: AppState) {
        super(props);
        const socket = io();
        socket.connect();

        socket.on("connect", (data) => {
            console.log("Connected to server");
        });
        socket.on("disconnect", (data) => {
            console.log("Disconnected from server");
        });
        socket.on("connect_error", (error) => {
            console.log("Error in the WS connection");
            console.log(error);
        });
        socket.on("error", (error) => {
            console.log("Error in the WS connection");
            console.log(error);
        });

        this.state = {
            socketContext: {socket}
        };
    }

    public render() {
        return (
            <div className="AppContainer">
                <SocketContext.Provider value={this.state.socketContext}>
                    <Router>
                        <React.Fragment>
                            <Navigation/>
                            <Switch>
                                <Route
                                    exact={true}
                                    path={routing.current}
                                    component={CurrentViewContainer}
                                />
                                <Route
                                    exact={true}
                                    path={routing.experiments}
                                    component={ExperimentsViewContainer}
                                />
                                <Route
                                    exact={true}
                                    path={routing.individuals}
                                    component={IndividualsViewContainer}
                                />
                                <Route
                                    path={routing.individual}
                                    component={IndividualViewContainer}
                                />
                                <Route
                                    exact={true}
                                    path={routing.database}
                                    component={DatabaseViewContainer}
                                />
                                <Route
                                    component={ControlViewContainer}
                                />
                            </Switch>
                        </React.Fragment>
                    </Router>
                </SocketContext.Provider>
            </div>
        );
    }
}

export default App;
