from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Numeric,
    String,
    create_engine,
)
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import io
import os

Base = declarative_base()


class Job(Base):
    __tablename__ = 'jobs'

    id = Column(Integer, primary_key=True)
    submission_delay = Column(Integer)
    mi = Column(Numeric)
    number_of_cores = Column(Integer)
    cpu_time_spent_s = Column(Numeric)
    mips_per_core = Column(Numeric)
    wallclock_time_spent_s = Column(Numeric)
    job_started = Column(Numeric)
    job_ended = Column(Numeric)
    extra_info = Column(String)


class Experiment(Base):
    __tablename__ = 'experiments'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    configuration_json = Column(String)
    populations = relationship('Population', backref='experiment')


class Population(Base):
    __tablename__ = 'populations'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    iterations = relationship('Iteration', backref='population')
    experiment_id = Column(Integer, ForeignKey('experiments.id'))


class Iteration(Base):
    __tablename__ = 'iterations'

    id = Column(Integer, primary_key=True)
    iteration_no = Column(Integer)
    population_id = Column(Integer, ForeignKey('populations.id'))
    individuals = relationship('Individual', backref='iteration')


class Individual(Base):
    __tablename__ = 'individuals'

    id = Column(Integer, primary_key=True)
    iteration_id = Column(Integer, ForeignKey('iterations.id'))
    fitness = Column(Numeric)
    genotype = Column(String)
    evaluation_time = Column(Numeric)
    parents = Column(String)
    genealogy_index = Column(Integer)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


db_host = os.environ.get('DB_HOST', 'postgres')
db_port = os.environ.get('DB_PORT', '5432')
db_name = os.environ.get('DB_NAME', 'dnnevo')
db_user = os.environ.get('DB_USER', 'dnnevo')
db_pass = os.environ.get('DB_PASS', 'dnnevo')

engine = create_engine(
    f'postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}',
)
Base.metadata.create_all(engine)


def load_model_genotype(session, model_id):
    wrapped = session.query(Individual.genotype).filter_by(id=model_id).first()
    return wrapped[0]


def load_database(db_dump):
    result = engine.execute(db_dump)

    str_result = ""
    try:
        for line in result:
            str_result += str(line) + '\n'
    finally:
        result.close()

    return str_result


def dump_database():
    conn = engine.raw_connection()
    dump = io.BytesIO()
    for line in conn.iterdump():
        line_with_newline = line + '\n'
        dump.write(line_with_newline.encode('utf-8'))
    dump.seek(0)
    return dump


# Refactor: wrap in a class
#
def get_db_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def get_individuals(session, offset, limit=10):
    if offset < 0:
        raise RuntimeError('Offset has to be >0!')
    return session.query(Individual).limit(limit).offset(offset).all()


def get_individuals_count(session):
    return session.query(Individual).count()


def get_individual(session, individual_id):
    return session.query(Individual).filter(Individual.id == individual_id).all()


def get_population_name_for_individual_id(session, individual_id):
    return session.query(Population.name).select_from(Individual).join(Iteration).join(
        Population).filter(Individual.id == individual_id).first()


def engine_dispose():
    engine.dispose()
