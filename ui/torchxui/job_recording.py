import logging
from torchxui.db import Job, engine_dispose

logger = logging.getLogger(__name__)


class DatabaseTaskObserver(object):

    def __init__(self, db_session):
        self._db_session = db_session
        self._active_jobs = {}

    def notify_started(self, internal_task_id, task_metadata):
        engine_dispose()
        job = Job(
            submission_delay=task_metadata['submission_delay'],
            number_of_cores=task_metadata['number_of_cores'],
            mips_per_core=task_metadata['mips_per_core'],
            job_started=task_metadata['job_started'],
            extra_info=task_metadata['extra_info'],
        )
        self._db_session.add(job)
        self._commit()
        self._active_jobs[internal_task_id] = job

    def notify_finished(self, internal_task_id, task_metadata):
        engine_dispose()
        job = self._active_jobs.get(internal_task_id)

        if job is None:
            logger.error(f'Incorrect state! Task with id: {internal_task_id} '
                         f'not found in active tasks map! THIS IS A CODE BUG')
            return

        mi = task_metadata['cpu_time_spent_s'] * task_metadata['mips_per_core']
        job.mi = mi
        job.wallclock_time_spent_s = task_metadata['wallclock_time_spent_s']
        job.cpu_time_spent_s = task_metadata['cpu_time_spent_s']
        job.job_ended = task_metadata['job_ended']

        self._commit()
        del self._active_jobs[internal_task_id]

    def _commit(self):
        try:
            self._db_session.commit()
            logger.debug("Committed session")
        except Exception as e:
            logger.error("Exception while committing data")
            logger.error(e)
            self._db_session.rollback()
