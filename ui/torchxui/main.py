import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s: %(levelname)s/%(name)s] %(message)s',
    handlers=[
        # logging.handlers.RotatingFileHandler(
        #     "torch-dnnevo.log",
        #     maxBytes=(1048576*5),
        #     backupCount=7,
        # ),
        logging.StreamHandler(sys.stdout),
    ])

logger = logging.getLogger(__name__)

import os
from torchxui.service import initalize_flask, initialize_socketio
from torchxui.db import get_db_session


DEBUG = os.environ.get('DEBUG', 'FALSE').upper() == 'TRUE'

def main():
    logger.info('Starting server with socketio')
    db_session = get_db_session()
    app = initalize_flask(db_session)
    socketio = initialize_socketio(app, db_session)
    logger.info('Started server with socketio')
    socketio.run(app, host='0.0.0.0', port=8080)

if __name__ == '__main__':
    main()
