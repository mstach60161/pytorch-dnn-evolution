import logging
import os
import time
import random

from celery import Celery, current_task, signals
from torchx.evo.utils.promises import ValuePromise
from torchx.evo.nn import ProgressTracker, evaluate_as_nn
from torchxui.job_recording import DatabaseTaskObserver
from torchxui.db import get_db_session

logger = logging.getLogger(__name__)

logger.info('Before setup')

IS_TEST_WORKLOAD = os.environ.get('IS_TEST_WORKLOAD', 'FALSE') == 'TRUE'
QUEUE_HOSTNAME = os.environ.get('QUEUE_HOSTNAME', 'queue')

__internal_task_id = 0
__task_observer = None
__extra_info = None


def set_extra_info(extra_info):
    global __extra_info
    __extra_info = extra_info


def register_task_observer(task_observer):
    global __task_observer
    __task_observer = task_observer


def get_mips_per_core():

    with open("/proc/cpuinfo", "r") as f:
        cpuinfo = f.readlines()

    bogomips = None
    for line in cpuinfo:
        if 'bogomips' in line:
            # Assuming all cores have the same MIPS
            bogomips = float(line.split(':')[1].strip())
            break

    if bogomips is None:
        raise RuntimeError("Could not read the bogomips from /proc/cpuinfo")

    return bogomips


# Number of cores is always 1 since a python process is single threaded
NUMBER_OF_CORES = 1
MIPS_PER_CORE = get_mips_per_core()

celery = Celery(
    'dnnevaluation',
    backend='rpc://',
    broker=f'pyamqp://{QUEUE_HOSTNAME}//',
    include=['torchxui.celery'],
)

celery.conf.update(
    broker_heartbeat=0,
    broker_pool_limit=None,
    broker_transport_options={'confirm_publish': True},
    broker_connection_timeout=30,
    broker_connection_retry=True,
    broker_connection_max_retries=100,
    task_acks_late=True,
    worker_prefetch_multiplier=1,
    task_track_started=True,
)

logger.info(f'Initialized the celery queue with following options: '
            f'{celery.conf}')


class CeleryRemotePromise(ValuePromise):

    def __init__(self,
                 remote_result,
                 ):
        self.__remote_result = remote_result
        self.__retrieved_result = None

    def _fetch_result(self):
        if self.__retrieved_result is None:
            if IS_TEST_WORKLOAD:
                def on_raw_message(body):
                    print(f'Retrieved raw message: {body}')

                self.__retrieved_result = self.__remote_result.get(
                    on_message=on_raw_message
                )
            else:
                self.__retrieved_result = self.__remote_result.get()

        return self.__retrieved_result

    def get_value(self):
        fitness_with_job = self._fetch_result()
        return fitness_with_job['value']

    def get_remote_result(self):
        return self.__remote_result

    def get_job_metadata(self):
        fitness_with_job = self._fetch_result()
        return fitness_with_job


class CeleryProgressTracker(ProgressTracker):

    def __init__(self):
        super().__init__()
        self._progress = 0

    def update_progress(self, updated_progress):
        self._progress = updated_progress

        current_task.update_state(
            state='PROGRESS',
            meta={
                'current': self._progress,
                'total': self.target_value
            }
        )


def clear_queue():
    logger.debug("Clearning the queue")
    celery.control.purge()


@signals.task_prerun.connect
def task_prerun(
        task_id=None,
        task=None,
        args=None,
        **kwargs,
    ):
    if __task_observer:
        internal_task_id = kwargs['kwargs']['internal_task_id']
        extra_info = kwargs['kwargs'].get('extra_info')
        submission_delay = time.time()
        initial_stats = {
            'mips_per_core': MIPS_PER_CORE,
            'submission_delay': submission_delay,
            'number_of_cores': NUMBER_OF_CORES,
            'job_started': submission_delay,
            'extra_info': extra_info,
        }

        __task_observer.notify_started(internal_task_id,
                                       initial_stats)


@signals.worker_init.connect
def worker_init(**kwargs):
    logger.info('Initializing job recording')
    db_session = get_db_session()
    task_observer = DatabaseTaskObserver(db_session)
    register_task_observer(task_observer)
    logger.info('Initialied job recording')


@signals.task_success.connect
def task_success(result=None, **kwargs):
    if __task_observer:
        internal_task_id = result['internal_task_id']
        __task_observer.notify_finished(internal_task_id, result)


@celery.task
def _evaluate_individual_remotely(internal_task_id,
                                  individual,
                                  fitness_predictor,
                                  parameters,
                                  extra_info):
    submission_delay = time.time()

    start_ts = time.process_time()
    if IS_TEST_WORKLOAD:
        pt = CeleryProgressTracker()

        for i in range(10):
            time.sleep(1)
            pt.update_progress(i*10.0)

        eval_result = 100.0 * random.random()
    else:
        eval_result = evaluate_as_nn(
            individual=individual,
            parameters=parameters,
            dataset_subset=fitness_predictor,
            progress_tracker=CeleryProgressTracker(),
        )

    end_ts = time.process_time()
    cpu_time_spent_s = end_ts - start_ts
    job_end = time.time()

    wallclock_time_spent_s = job_end - submission_delay

    return {
        'value': (eval_result, ),
        'number_of_cores': NUMBER_OF_CORES,
        'mips_per_core': MIPS_PER_CORE,
        'wallclock_time_spent_s': wallclock_time_spent_s,
        'cpu_time_spent_s': cpu_time_spent_s,
        'job_ended': job_end,
        'internal_task_id': internal_task_id,
        'extra_info': extra_info,
    }


def remote_evaluate_model(model_genotype,
                          dataset_subset,
                          parameters,
                          ):
    global __internal_task_id
    __internal_task_id += 1

    remote_result = _evaluate_individual_remotely.delay(
        internal_task_id=__internal_task_id,
        individual=model_genotype,
        fitness_predictor=dataset_subset,
        parameters=parameters,
        extra_info=__extra_info,
    )

    logger.debug(f'Returning the remote result: type: {type(remote_result)} value: {remote_result}')
    return CeleryRemotePromise(remote_result)
