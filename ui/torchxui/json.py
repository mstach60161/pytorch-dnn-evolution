from uuid import UUID
from datetime import datetime
from decimal import Decimal
from torchx.evo.genotype import Genotype
from torchx.evo.intermediate import Layer
import time
import json

JSONEncoder_olddefault = json.JSONEncoder.default


def JSONEncoder_newdefault(self, o):
    if isinstance(o, UUID):
        return str(o)
    if isinstance(o, datetime):
        return str(o)
    if isinstance(o, time.struct_time):
        return datetime.fromtimestamp(time.mktime(o))
    if isinstance(o, Decimal):
        return str(o)
    if isinstance(o, Layer):
        return {
            "id": o.id,
            "inputs": o.inputs,
            "outputs": o.outputs,
            "extras": o.extras
        }
    if isinstance(o, Genotype):
        res = []
        for i in range(o.size):
            gene = o.gene(i)
            res.append({"id": i,
                        "x": gene.x,
                        "y": gene.y,
                        "extra": gene.extra,
                        "gene_type": "output" if gene.is_output() else "input",
                        "gene_type_value": gene.type_value})
        return res
    return JSONEncoder_olddefault(self, o)


def setup_json_serialization():
    json.JSONEncoder.default = JSONEncoder_newdefault
