from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import TypeVar, Generic, Tuple, List, Union

T = TypeVar('T')


class ValuePromise(ABC, Generic[T]):
    @abstractmethod
    def get_value(self) -> Union[T, Tuple[T, ...]]: ...


@dataclass
class MemoryValuePromise(Generic[T], ValuePromise[T]):
    _value: T

    def get_value(self) -> T:
        return self._value


@dataclass
class TargetDiffValuePromise(Generic[T], ValuePromise[T]):
    _promise: ValuePromise[T]
    _target: T

    def _adjusted_target(self):
        maybe_collection = self._target
        if isinstance(maybe_collection, tuple):
            return self._target

        if isinstance(maybe_collection, list):
            return self._target

        return (self._target, )

    def get_value(self) -> Tuple[T]:
        wrapped_value = self._promise.get_value()

        target_diff = [
            abs(target - wrapped)
            for target, wrapped
            in zip(self._adjusted_target(), wrapped_value)
        ]

        return tuple(target_diff)


@dataclass
class AvgCompositeValuePromise(ValuePromise[Union[int, float]]):
    _promises: List[ValuePromise[Union[int, float]]]

    def get_value(self) -> Tuple[float]:
        tpls_cnt = len(self._promises)

        # assuming that every tuple has the same size
        avgs_cnt = len(self._promises[0].get_value())

        tuples = [prmse.get_value() for prmse in self._promises]

        result = []
        for i in range(avgs_cnt):
            position_sum = sum([tpl[i] for tpl in tuples])
            avg = float(position_sum) / tpls_cnt
            result.append(avg)

        return tuple(result)

