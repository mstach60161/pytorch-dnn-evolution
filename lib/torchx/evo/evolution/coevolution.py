import logging
import statistics
from typing import Tuple, Dict, Generic

from torchx.evo.evolution.configuration import DNNEvoConfiguration
from torchx.evo.evolution.process import PopulationEvolution, Evaluator, Trainer, EvaluateFunc
from torchx.evo.evolution.toolbox import ToolboxBuilder, Population
from torchx.evo.utils.iteration_tracker import CompositeIterationTracker
from torchx.evo.utils.promises import TargetDiffValuePromise

logger = logging.getLogger('evolution')


class CoEvolution(Generic[Trainer]):

    def __init__(
            self,
            name: str,
            evaluate_with_trainer: EvaluateFunc[Trainer],
            configuration: DNNEvoConfiguration
    ):
        self._log = logger.getChild(name)

        builder = ToolboxBuilder(configuration)
        main_toolbox, fp_toolbox = builder.build()

        self._main_evolution_parameters = \
            configuration.main_train_config.to_dict()

        fp_evolution_parameters = configuration.fp_train_config.to_dict()

        def evaluate_fp_compare_to_trainer(fp, nn, par):
            fitness_val_promise = evaluate_with_trainer(nn, fp, par)
            trainer_fitness = nn.fitness.values
            return TargetDiffValuePromise(
                fitness_val_promise,
                trainer_fitness
            )

        self._main_evolution = PopulationEvolution(
            f"{name}_main",
            main_toolbox,
            Evaluator(evaluate_with_trainer, self._main_evolution_parameters)
        )
        self._fp_evolution = PopulationEvolution(
            f"{name}_fp",
            fp_toolbox,
            Evaluator(evaluate_fp_compare_to_trainer, fp_evolution_parameters)
        )

        # need to initialize evaluators
        self._evaluate_with_trainer = evaluate_with_trainer

        self.__initialize_trainers()
        self._stale_iterations_cnt = configuration.stale_iterations_cnt
        self.iteration = CompositeIterationTracker(self._log, [
            self._main_evolution.iteration,
            self._fp_evolution.iteration,
        ])

    def __initialize_trainers(self):
        best_nn = self._main_evolution.best()
        self._log.debug(f"Initializing co-evolution: setting FP trainer: id: {id(best_nn)}")
        del best_nn.fitness.values
        self._fp_evolution.evaluator.trainers.append(best_nn)

        best_fp = self._fp_evolution.best()
        self._log.debug(f"Initializing co-evolution: setting MAIN trainer: id: {id(best_fp)}")
        del best_fp.fitness.values
        self._main_evolution.evaluator.trainers.append(best_fp)

    def _should_add_trainers(self):
        """
        Condition which triggers trainers recomputation - checked every
        iteration
        """

        if self.iteration.current_no < self._stale_iterations_cnt:
            return False

        fitness = self._main_evolution.max_fitness(self._stale_iterations_cnt)
        prev = fitness[0]
        for f in fitness[1:]:
            if f != prev:
                return False
            prev = f

        return True

    def _select_new_fp_trainer(self):
        """
        Chooses the solution (neural network) which has the highest variance
        among the fitness predictors. This means that the fitness predictors
        have a problem with evaluating this individual - and we want them to be
        as good as possible at this. Finally, the chosen NN is evaluated
        against the full data set.
        """

        with self.iteration.with_special_section() as iteration_section:
            main_toolbox = self._main_evolution.toolbox
            main_population = self._main_evolution.population
            fp_population = self._fp_evolution.population

            # +1 at the end for the final evaluation of the trainer on the whole
            # dataset
            iteration_section.set_target(len(main_population) * len(fp_population) + 1)

            def evaluate(neural_network, fitness_predictor):
                return self._evaluate_with_trainer(
                    neural_network,
                    fitness_predictor,
                    self._main_evolution_parameters
                )

            promises = []
            for nn in main_population:
                def evaluate_solution(fitness_predictor):
                    return evaluate(nn, fitness_predictor)

                promise = main_toolbox.map(
                    evaluate_solution,
                    fp_population
                )
                promises.append(promise)

            def retrieve_value(promise):
                value = promise.get_value()
                iteration_section.tick()
                self._log.debug(f"FP Trainer selection: retrieved fitness: {value}")
                self._log.debug(f"FP Trainer selection: progress: {self.iteration.current_progress}")
                return value

            variances = []
            for nn_promises in promises:
                fitnesses = main_toolbox.map(
                    lambda p: retrieve_value(p)[0],
                    nn_promises
                )
                variances.append(statistics.variance(fitnesses))

            trainer_idx = variances.index(max(variances))

            fp_trainer = main_population[trainer_idx]
            fp_trainer = main_toolbox.clone(fp_trainer)

            return fp_trainer

    def __log_trainers_fitness(self):
        for fp_trainer in self._fp_evolution.evaluator.trainers:
            self._log.debug("FP Trainer: {} id: {}".format(
                str(fp_trainer),
                str(id(fp_trainer))
            ))

            self._log.debug("FP Trainer full dataset fitness value: " +
                         str(fp_trainer.fitness.values))

    def evolve(self):
        self._log.debug("Scheduling training of FP trainers on full dataset")
        fp_trainers_with_promises = []
        for fp_trainer in self._fp_evolution.evaluator.trainers:
            if not fp_trainer.fitness.valid:
                # get the initial fitness on the whole dataset
                promise = self._evaluate_with_trainer(
                    fp_trainer,
                    [],  # full dataset
                    self._main_evolution_parameters
                )
                fp_trainers_with_promises.append((fp_trainer, promise))

        self._log.debug("Scheduling MAIN population evolution calculation")
        self._main_evolution.evolve()

        self._log.debug("Assigning fitness to FP trainers")
        for fp_trainer, fp_trainer_promise in fp_trainers_with_promises:
            fp_trainer.fitness.values = fp_trainer_promise.get_value()

        self._fp_evolution.evolve()

        if self._should_add_trainers():
            fp_trainer = self._select_new_fp_trainer()
            self._fp_evolution.evaluator.trainers.append(fp_trainer)

        # update the fitness predictors for main population in every iteration
        best_fp = self._fp_evolution.best()
        self._main_evolution.evaluator.trainers.append(best_fp)

    def population(self) -> Tuple[Population, Population]:
        return (
            self._main_evolution.population,
            self._fp_evolution.population,
        )

    def fitness_history(self) -> Dict:
        return {
            'main': self._main_evolution.fitness_history(),
            'fp': self._fp_evolution.fitness_history(),
        }

    def last_fitness_update(self) -> Dict:
        return {
            'main': self._main_evolution.last_fitness_update(),
            'fp': self._fp_evolution.last_fitness_update(),
        }

    @property
    def individual_history(self) -> Dict:
        return {
            'fp': self._fp_evolution.history,
            'main': self._main_evolution.history
        }
