from torchx.evo.evolution.coevolution import CoEvolution
from torchx.evo.evolution.configuration import Configuration, ConfigurationDict, \
    DNNEvoConfiguration, DNNTrainConfiguration
from torchx.evo.evolution.process import Evaluator, PopulationEvolution
from torchx.evo.evolution.toolbox import ToolboxBuilder, Individual
