import logging
import random
from dataclasses import dataclass
from typing import Tuple, Optional, List

from deap import base, creator, tools

from torchx.evo.datasets import retrieve_dataset
from torchx.evo.evolution.configuration import DNNEvoConfiguration
from torchx.evo.genotype import NUMBER_OF_FIELDS

logger = logging.getLogger('evolution')


def signature(trainers_population):
    """
    Creates a hash for the population of trainers - in reality a list of lists.
    Order of trainers doesn't matter - so we sort the trainers within the
    population.
    """
    as_tuples = tuple(sorted([tuple(i) for i in trainers_population]))
    return hash(as_tuples)


def mut_uniform(individual, indpb):
    """This function applies a uniform mutation on the input individual. This
    mutation expects a :term:`sequence` individual composed of real valued
    attributes. The *indpb* argument is the probability of each attribute to be
    mutated.

    :param individual: Individual to be mutated.
    :param indpb: Independent probability for each attribute to be mutated.
    :returns: A tuple of one individual.

    This function uses the :func:`~random.random` functions from the python
    base :mod:`random` module.
    """
    for i in range(len(individual)):
        if random.random() < indpb:
            individual[i] = random.random()

    return individual,


class TrainerAwareFitness(base.Fitness):

    def __init__(self, values=(), train_sign=None):
        super().__init__(values)
        self._train_sign = train_sign

    @property
    def train_sign(self) -> Optional[None]:
        """Signature of the trainers used to generate the fitness values"""
        return self._train_sign

    @train_sign.setter
    def train_sign(self, value):
        self._train_sign = value

    @train_sign.deleter
    def train_sign(self):
        self._train_sign = None

    def __hash__(self):
        return hash((self.wvalues, self.train_sign))

    def _raise_if_different_trainers(self, other):
        if other.train_sign != self.train_sign:
            raise ValueError(
                "Trying to compare fitness values generated with use of different trainers! "
                f"Left: {other.train_sign} Right: {self.train_sign}"
            )

    def __le__(self, other):
        self._raise_if_different_trainers(other)
        return self.wvalues <= other.wvalues

    def __lt__(self, other):
        self._raise_if_different_trainers(other)
        return self.wvalues < other.wvalues

    def __eq__(self, other):
        self._raise_if_different_trainers(other)
        return self.wvalues == other.wvalues

    def __deepcopy__(self, memo):
        """Replace the basic deepcopy function with a faster one.
        It assumes that the elements in the :attr:`values` tuple are
        immutable and the fitness does not contain any other object
        than :attr:`values` and :attr:`weights`.
        """
        cp = self.__class__()
        cp.wvalues = self.wvalues
        cp.train_sign = self.train_sign
        return cp

    def __str__(self):
        """Return the values and trainer signature of the Fitness object."""
        values = self.values if self.valid else tuple()
        return f"{values} train_sign: {self.train_sign}"

    def __repr__(self):
        """Return the Python code to build a copy of the object."""
        mod = self.__module__
        cls = self.__class__.__name__
        values = self.values if self.valid else tuple()
        return f"{mod}.{cls}({values!r}, {self.train_sign!r})"


class Individual(list):
    fitness: TrainerAwareFitness


Population = List[Individual]


@dataclass
class ToolboxBuilder:
    configuration: DNNEvoConfiguration

    def build(self) -> Tuple[base.Toolbox, base.Toolbox]:
        return self._build_main_toolbox(), self._build_fp_toolbox()

    def _build_fp_toolbox(self):
        """
        Fitness Predictor population configuration
        """

        fp_toolbox = base.Toolbox()

        # fitness predictors minimize the value - it is the difference to the
        # trainer fitness on full dataset
        creator.create("FPFitness", TrainerAwareFitness, weights=(-1.0,))
        creator.create("FPIndividual", Individual, fitness=creator.FPFitness)

        config = self.configuration.fp_train_config
        dataset = retrieve_dataset(config.dataset)
        max_sample_id = dataset.samples_count - 1

        fp_toolbox.register("map", lambda f, i: list(map(f, i)))
        fp_toolbox.register("attr_int", random.randint, 0, max_sample_id)
        fp_toolbox.register("individual",
                            tools.initRepeat,
                            creator.FPIndividual,
                            fp_toolbox.attr_int,
                            n=self.configuration.fp_individual_size)
        fp_toolbox.register("population", tools.initRepeat,
                            list,
                            fp_toolbox.individual,
                            self.configuration.fp_population_size)
        fp_toolbox.register("mate", tools.cxTwoPoint)
        fp_toolbox.register("mutate", tools.mutUniformInt,
                            low=0, up=max_sample_id,
                            indpb=self.configuration.fp_population_size)
        fp_toolbox.register("select", tools.selTournament, tournsize=2)
        fp_toolbox.register("mut_probability",
                            lambda: self.configuration.fp_mutation_probability)
        fp_toolbox.register("cx_probability",
                            lambda:
                            self.configuration.fp_crossover_probability)

        return fp_toolbox

    def _build_main_toolbox(self):
        """
        Main population configuration

        Main fitness is looking for highest value - fitness is recognition
        accuracy on test set (so 100% is max)
        """
        creator.create("MainFitness", TrainerAwareFitness, weights=(1.0,))
        creator.create("MainIndividual", Individual, fitness=creator.MainFitness)

        individual_size = self.configuration.main_individual_size
        genotype_size = individual_size * NUMBER_OF_FIELDS

        main_toolbox = base.Toolbox()
        main_toolbox.register("map", lambda f, i: list(map(f, i)))
        main_toolbox.register("attr_float", random.random)
        main_toolbox.register("individual",
                              tools.initRepeat,
                              creator.MainIndividual,
                              main_toolbox.attr_float,
                              n=genotype_size)
        main_toolbox.register("population", tools.initRepeat, list,
                              main_toolbox.individual,
                              self.configuration.main_population_size)
        main_toolbox.register("mate", tools.cxTwoPoint)
        main_toolbox.register("mutate", mut_uniform,
                              indpb=
                              self.configuration.main_mutation_probability)
        main_toolbox.register("select",
                              tools.selTournament, tournsize=2)
        main_toolbox.register("mut_probability",
                              lambda:
                              self.configuration.main_mutation_probability)
        main_toolbox.register("cx_probability",
                              lambda:
                              self.configuration.main_crossover_probability)

        return main_toolbox
