import celery


def list_args(lst):
    for i in lst:
        print(f"{i['id']} {i['args']}")


app = celery.Celery(
    'dnnevaluation',
    backend='rpc://',
    broker='pyamqp://localhost//',
    include=['torchxui.celery'],
)

inspect = app.control.inspect()
active = inspect.active()['celery@dnnevoworker']
reserved = inspect.reserved()['celery@dnnevoworker']

print('Active')
list_args(active)
print('Reserved')
list_args(reserved)

print(f'Stats: {len(active)} {len(reserved)}')
